const uuid = require("uuid");
const { encrypt } = require('../utils');

// In memory Users db
const users = {};

/**
 * generates and sets a unique id on the new User instance
 * @class
 */
class User {
  constructor(username, email) {
    this.id = uuid();
    this.username = username;
    this.email = email;
    users[this.id] = this;
  }

  /**
   * find user by id
   *
   * @param  {String} id - user id
   * @returns  {User} - matching user instance
   */
  static get(id) {
    return users[id];
  }

  /**
   * delete user by id
   *
   * @param  {String} id - user id
   */
  static delete(id) {
    delete users[id];
  }

  /**
   * set user password - encrypts password for storage
   *
   * @param  {String} password - user password to be saved
   * @returns  {User} - matching user instance
   */
  async setPassword(password) {
    this.password = await encrypt(password);
  }

  /**
   * serialize user to JSON, excluding password hash
   *\
   * @returns  {String} - JSON serialized user
   */
  toJSON() {
    return JSON.stringify({
      id: this.id,
      username: this.username,
      email: this.email
    });
  }
}

module.exports = {
  User,
  users
};
