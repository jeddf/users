const express = require("express");
const {
  getUser: getUserHandler,
  deleteUser: deleteUserHandler,
  createUser: createUserHandler
} = require("../handlers");
const { adminCheck: adminCheckMiddleware } = require("../middleware");

const user = new express.Router();

user.post("/", createUserHandler);
user.delete("/:id", adminCheckMiddleware, deleteUserHandler);
user.get("/:id", getUserHandler);

module.exports = {
  user
};
