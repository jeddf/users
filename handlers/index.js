const {
  User
} = require("../db");
const { getCodeResponse } = require('../utils');

const createUser = async (req, res, next) => {
  const { username, email, password } = req.body;
  const user = new User(username, email);
  try {
    await user.setPassword(password)
  } catch (error) {
    return next(error);
  }
  res.status(201);
  res.json(user);
};

const deleteUser = (req, res) => {
  const { id } = req.params;
  if (!User.get(id)) {
    res.status(404);
    res.json(getCodeResponse(404));
  } else {
    User.delete(id);
    res.sendStatus(204);
  }
};

const getUser = (req, res) => {
  const { id } = req.params;
  const user = User.get(id);
  if (!user) {
    res.status(404);
    res.json(getCodeResponse(404));
  } else {
    res.status(200);
    res.json(user);
  }
};

module.exports = {
  createUser,
  deleteUser,
  getUser
};
