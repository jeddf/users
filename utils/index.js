const statusLib = require('http-status');
const bcrypt = require("bcrypt");
const SALT_ROUNDS = 12;

/**
 * salt and hash a string using bcrypt library
 *
 * @param  {String} input - string to be hashed
 * @returns  {String} - hashed string
 */
const encrypt = async input => {
  const salt = await bcrypt.genSalt(SALT_ROUNDS);
  return await bcrypt.hash(input, salt);
};

/**
 * build response object for status code
 *
 * @param  {Number} input - status code
 * @returns  {Object} - code response
 */
const getCodeResponse = (code) => {
  const message = statusLib[code];
  return {
    code,
    message
  };
}

module.exports = {
  encrypt,
  getCodeResponse
};
