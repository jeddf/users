const express = require("express");
const bodyParser = require("body-parser");
const morgan = require('morgan');

const { user: userRouter } = require('./routers');
const { errorHandler, notFoundHandler } = require('./middleware');

const PORT = process.env.PORT || 3000;
const isProduction = process.env.NODE_ENV === 'production';

const app = express();
app.use(bodyParser.json());
if (!isProduction) {
    app.use(morgan('dev'));
    app.use(express.static('apidocs'));
}

app.use('/api/v1/user', userRouter);

app.use(notFoundHandler);
app.use(errorHandler);

app.listen(PORT, () => {
    console.log(`Listening on port ${PORT}`)
});