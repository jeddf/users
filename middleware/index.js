const { getCodeResponse } = require('../utils');

const ADMIN_SECRET = process.env.NODE_USER_ADMIN_SECRET;

const adminCheck = (req, res, next) => {
  const { "x-user-admin-secret": secretIn } = req.headers;
  if (secretIn !== ADMIN_SECRET) {
    res.status(403);
    res.json(getCodeResponse(403));
  } else {
    next();
  }
};

const notFoundHandler = (req, res) => {
  res.status(404);
  res.json(getCodeResponse(404));
};

const errorHandler = (error, req, res, next) => {
  res.status(500);
  res.json(getCodeResponse(500));
  if (error.stack) {
    console.log(error.stack);
  }
};

module.exports = {
  adminCheck,
  errorHandler,
  notFoundHandler
};
