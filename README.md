# Users API
HTTP REST API server for creating, reading and deleting users.

## Environment Variables
`NODE_USER_ADMIN_SECRET` - Secret for admin priveleges

`PORT`

## Running
`npm install`

`node server.js`